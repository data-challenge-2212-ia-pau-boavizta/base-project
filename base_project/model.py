
class DummyModel:

    def __init__(self):
        pass

    def predict(self, text: str) -> int:
        return len(text) % 2


if __name__ == '__main__':
    dummy_model = DummyModel()
    prediction = dummy_model.predict('I hated this film so much!!!')
    print('Prediction of the dummy model is:', prediction)
