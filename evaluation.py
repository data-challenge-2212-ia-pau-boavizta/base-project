import os
import time

import pandas as pd

from base_project.model import DummyModel


TEST_DATASET_PATH = os.environ['TEST_DATASET_TEXT_PATH']


def evaluate():
    df = pd.read_csv(TEST_DATASET_PATH)
    model = DummyModel()

    predictions = []
    for row in df.itertuples():
        output = model.predict(row.text)
        predictions.append({
            'id': row.id,
            'label': output
        })

    time.sleep(60)

    pd.DataFrame(predictions).to_csv('prediction.csv', index=False)


if __name__ == '__main__':
    evaluate()
